import RPi.GPIO as GPIO
import random
import string
from websocket import create_connection
from mfrc522 import SimpleMFRC522

reader = SimpleMFRC522()

print("Start Python Client")
print("Open Connection to Server")
ws = create_connection("ws://192.168.0.100:9090")
helloServerMsg = ws.recv()
print("Received '%s'" % helloServerMsg)

print("Sending 'test'")
ws.send("test")
result = ws.recv()
print("Received '%s'" % result)


def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

print("Sending random messages")
while True:
        try:

                id,text = reader.read()
                ws.send(str(id))
                result = ws.recv()
                print("Received from server: '%s'" % result)

        finally:
                GPIO.cleanup()

print("Connection to WebSocker Server Closed")
ws.close()